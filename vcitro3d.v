module vcitro3d

#include <citro3d.h>
pub const (
	c3d_default_cmdbuf_size = 0x40000
	c3d_frame_syncdraw      = byte(1)
	c3d_frame_nonblock      = byte(2)
)

fn C.C3D_Init(int)

fn C.C3D_Fini()

fn C.C3D_FrameBegin(byte)

fn C.C3D_FrameEnd(byte)

pub fn init(cmdbuf_size int) {
	C.C3D_Init(cmdbuf_size)
}

pub fn fini() {
	C.C3D_Fini()
}

pub fn frame_begin(drawmode byte) {
	C.C3D_FrameBegin(drawmode)
}

pub fn frame_end() {
	C.C3D_FrameEnd(0)
}
